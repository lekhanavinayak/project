package com.example.project1.controller;

import com.example.project1.entity.InvoiceDetials;
import com.example.project1.entity.InvoiceInfo;
import com.example.project1.repository.InvoiceInfoRepo;
import com.example.project1.service.InvoiceInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@Slf4j
public class InvoiceInfoController {

    @Autowired
    InvoiceInfoService invoiceInfoService;

    @Autowired
    InvoiceInfoRepo invoiceInfoRepo;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");
    Date date = new Date();
    String date1=formatter.format(date);

    @GetMapping(path = {"/invoice"})
    public String getInvoiceForm(Model model) {

        model.addAttribute("invoice_date",date1 );
        return "invoice_information";
    }
    @PostMapping("/saveInvoice")
    public String saveInvoice(@ModelAttribute("invoice_information") @Valid InvoiceInfo invoice_information,
                                     @ModelAttribute("invoice_details") @Valid InvoiceDetials invoice_details ,
                                     @RequestParam("myfile") MultipartFile myfile, HttpServletRequest request ) throws IOException {
        invoice_details.setFilename(myfile.getOriginalFilename());
        invoice_details.setFiletype(myfile.getContentType());
        invoice_details.setFileByte(myfile.getBytes());

        invoiceInfoService.saveInvoiceInfo(invoice_information);
        invoiceInfoService.saveInvoiceDetails(invoice_details);

        String inv = invoice_information.getInvoice();
        String product[] = request.getParameterValues("product");
        String description[] = request.getParameterValues("description");
        String price[] = request.getParameterValues("price");
        String qty[] = request.getParameterValues("qty");
        String tax[] = request.getParameterValues("tax");
        for (int i = 0; i < product.length - 1; i++) {
            invoice_information.addDetails(product[i], description[i], price[i], qty[i], tax[i], inv);
        }
        System.out.println(invoice_information.getInvoice_details().size());
        invoiceInfoRepo.save(invoice_information);
        return "redirect:/invoicelist";

    }

}
