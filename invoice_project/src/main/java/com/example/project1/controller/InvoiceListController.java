package com.example.project1.controller;


import com.example.project1.entity.InvoiceInfo;
import com.example.project1.repository.InvoiceInfoRepo;
import com.example.project1.service.InvoiceInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@Slf4j
public class InvoiceListController {

    @Autowired
    InvoiceInfoService invoiceInfoService;

    @Autowired
    InvoiceInfoRepo invoiceInfoRepo;

    @GetMapping(path = "/invoicelist")
    public ModelAndView getAllInvoice() {

        ModelAndView mav = new ModelAndView("invoice_info_list");

        mav.addObject("invoicelists",invoiceInfoRepo.findAll());
        return mav;
    }
    @GetMapping("/showForUpdate/{invoice}")
    public String showForUpdate(@PathVariable(value = "invoice") String invoice, Model model) {

        InvoiceInfo invoiceInfo = invoiceInfoService.getInvoiceById(invoice);
        System.out.println(invoiceInfo.getInvoice()+":"+invoiceInfo.getInvoice_date()+":"+invoiceInfo.getBill_to());

        model.addAttribute("invoiceInfo", invoiceInfo);
        return "edit_invoice_info";
    }

    @RequestMapping("/deleteInvoice/{invoice}")
    public String deleteInvoice(@PathVariable(value = "invoice") String invoice, Model model) {

        invoiceInfoService.deleteInvoiceById(invoice);

        return "redirect:/invoicelist";
    }

    @PostMapping("/updateInvoice")
    public String updateInvoice(@ModelAttribute("invoice_information") @Valid InvoiceInfo invoice_information) {
        invoiceInfoService.deleteInvoice(invoice_information);
        invoiceInfoService.saveInvoiceInfo(invoice_information);

        return "redirect:/invoicelist";
    }

}
