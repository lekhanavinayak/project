package com.example.project1.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor

public class InvoiceDetials {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String invNo;
    private String product;
    private String description;
    private String price;
    private String qty;
    private String tax;
    private String grand_total1;

@ManyToOne
InvoiceInfo invoiceInfo;
    private String invoice_note;
    private String filename;
    private String filetype;
    @Lob
    private byte[] fileByte;
    public InvoiceDetials(String s, String s1, String s2, String s3, String tax, String inv, InvoiceInfo invoiceInfo) {
    }
}
