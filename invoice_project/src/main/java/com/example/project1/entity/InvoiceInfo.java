package com.example.project1.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int invoice_Id;
    private String bill_to;
    private String invoice_date;
    private String invoice;
    private String due_date;
    private String status;
    private  String grand_total1;
    @OneToMany(cascade = CascadeType.PERSIST,mappedBy = "invoiceInfo")
    List<InvoiceDetials> invoice_details=new ArrayList<>();

    public void addDetails(String s, String s1, String s2, String s3, String tax, String inv) {
        this.invoice_details.add(new InvoiceDetials(s,s1,s2,s3,tax,inv,this));
    }



}
