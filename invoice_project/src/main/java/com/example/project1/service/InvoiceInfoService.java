package com.example.project1.service;


import com.example.project1.entity.InvoiceDetials;
import com.example.project1.entity.InvoiceInfo;
import com.example.project1.repository.InvoiceDetailsRepo;
import com.example.project1.repository.InvoiceInfoRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class InvoiceInfoService {

    @Autowired
    InvoiceInfoRepo invoiceInfoRepo;
    @Autowired
    InvoiceDetailsRepo invoiceDetailsRepo;

    public void saveInvoiceInfo( InvoiceInfo invoiceInfo) {
            invoiceInfoRepo.save(invoiceInfo);

    }

    public void saveInvoiceDetails(InvoiceDetials invoice_details) {
        invoiceDetailsRepo.save(invoice_details);
    }


    public InvoiceInfo getInvoiceById(String invoice) {
        InvoiceInfo invoiceInfo = invoiceInfoRepo.findByInvoiceId(invoice);

        return invoiceInfo;

    }

    public void deleteInvoiceById(String invoice) {
        invoiceInfoRepo.deleteByInvoiceId(invoice);
        invoiceDetailsRepo.deleteByInvoiceId(invoice);
    }

    public void deleteInvoice( InvoiceInfo invoiceInfo) {
        invoiceInfoRepo.deleteByInvoiceId(invoiceInfo.getInvoice());

    }

}
