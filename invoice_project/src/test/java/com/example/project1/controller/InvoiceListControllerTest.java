package com.example.project1.controller;

import com.example.project1.service.InvoiceInfoService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.util.Assert;

import java.io.IOException;

@SpringBootTest
@ActiveProfiles("test")
public class InvoiceListControllerTest {

    @Autowired
    private InvoiceInfoService invoiceInfoService;

    @Autowired
    private InvoiceListController invoiceListController;

    @Test
    public void showForUpdateTest() throws IOException {
        String invoice=new String();

        invoiceListController.showForUpdate(invoice,new ExtendedModelMap());

    }
}
