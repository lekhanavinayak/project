package com.example.project1.service;

import com.example.project1.entity.InvoiceDetials;
import com.example.project1.entity.InvoiceInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.Assert;

import java.io.IOException;

@SpringBootTest
@ActiveProfiles("test")
public class SaveInvoiceInfoTest {
    @Autowired
    private InvoiceInfoService invoiceInfoService;

    @Test
    public void save() throws IOException {

        InvoiceInfo invoiceInfo = new InvoiceInfo();

        invoiceInfoService.saveInvoiceInfo(invoiceInfo);

    }
    @Test
    public void delete() throws IOException {

        String invoice=new String();

        invoiceInfoService.deleteInvoiceById(invoice);

    }
    @Test
    public void deleteInvoice() throws IOException {

        InvoiceInfo invoiceInfo=new InvoiceInfo();
        invoiceInfoService.deleteInvoice(invoiceInfo);

    }
    @Test
    public void saveInvoiceDetails() throws IOException {
        InvoiceDetials invoice_details=new InvoiceDetials();

        invoiceInfoService.saveInvoiceDetails(invoice_details);

    }
}
