package com.meeting.meeting_project.controller;

import com.meeting.meeting_project.entity.Event;
import com.meeting.meeting_project.exception.BadDateFormatException;
import com.meeting.meeting_project.repo.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Controller
public class EventController {

    @Autowired
    private EventRepository eventRepository;
    @RequestMapping(value="/event", method=RequestMethod.POST)
    public Event addEvent(@RequestBody Event event) {
        Event created = eventRepository.save(event);
        return created;
    }
    @RequestMapping(value = "/allevents", method= RequestMethod.GET)
    @ResponseBody
    public List<Event> allMeeting() {

        List<Event> list = (List<Event>) eventRepository.findAll();
        return list;

    }
}
