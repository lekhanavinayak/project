package com.meeting.meeting_project.repo;

import com.meeting.meeting_project.entity.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
//    List<Event> findAll();
//    Event save(Event event);
//    void delete(Event event);
//
//    @Query("select b from Event b " +
//            "where b.start between ?1 and ?2 and b.end between ?1 and ?2")
//    //List<Event> findByDatesBetween(Date start, Date end);
//
//    List<Event> findByDateBetween(LocalDateTime startDateTime, LocalDateTime endDateTime);
}
