package com.meeting.meeting_project.service;

import com.meeting.meeting_project.controller.EmployeeController;
import com.meeting.meeting_project.entity.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

@SpringBootTest
@ActiveProfiles("test")
public class ServiceTest {
    @Autowired
    private EmpService empService;
    @Autowired
    private EmployeeController employeeController;
    @Test
   /* public void deleteInvoice() throws IOException {
        long id =new long();
        empService.deleteEmployeeById(id);

    }*/
    public void save() throws IOException {
        Employee employee = new Employee();
        empService.saveEmployee(employee);

    }

}
